package com.viettel.bccs.cus.repo;

import com.viettel.bccs.cus.model.CusSubscriber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CusSubscriberRepo extends JpaRepository<CusSubscriber, Long> {

    List<CusSubscriber> findAllByIsdnAndStatus(String isdn, Long status);
}
