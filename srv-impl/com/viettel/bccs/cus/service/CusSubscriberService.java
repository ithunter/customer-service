package com.viettel.bccs.cus.service;

import com.viettel.bccs.cus.model.CusSubscriber;
import com.viettel.bccs.cus.repo.CusSubscriberRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CusSubscriberService {

    private final Logger logger = LoggerFactory.getLogger(CusSubscriberService.class);

    @Autowired
    CusSubscriberRepo cusSubscriberRepo;

    @GetMapping("/{isdn}/{status}")
    public ResponseEntity<List<CusSubscriber>> getSubscriber(@PathVariable("isdn") String isdn, @PathVariable("status") Long status) {
        logger.debug("Receive msg: isdn = " + isdn + "; status = " + status);
        List<CusSubscriber> subscribers = cusSubscriberRepo.findAllByIsdnAndStatus(isdn, status);
        return ResponseEntity.ok().body(subscribers);
    }
}
