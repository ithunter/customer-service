package com.viettel.bccs.cus.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "SUBSCRIBER")
public class CusSubscriber {

    @Id
    @Column(name = "SUB_ID")
    private Long subId;

    @Column(name = "CONTRACT_ID")
    private Long contractId;

    @Column(name = "ISDN")
    private String isdn;

    @Column(name = "IMSI")
    private String imsi;

    @Column(name = "SERIAL")
    private String serial;

    @Column(name = "status")
    private Long status;

    @Column(name = "PRODUCT_CODE")
    private String productCode;

    @Column(name = "OFFER_ID")
    private Long offerId;

    @Column(name = "ACT_STATUS")
    private String actStatus;

    @Column(name = "ACTIVE_DATETIME")
    private Date activeDate;

    @Column(name = "SHOP_CODE")
    private String shopCode;

    @Column(name = "PAY_TYPE")
    private String payType;
}
