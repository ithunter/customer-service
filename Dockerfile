################# Run with TLH Team Image #############
# Get TLH Team Image with already lib-fw
FROM thanhdc/tlhteam:psimage.1.0 as backend

# Working directory
WORKDIR /u01/app/bccs/os_gem/app

# Copy srcCode for build
COPY . /u01/app/bccs/os_gem/app/src

RUN ls -la /u01/app/bccs/os_gem/app

# Build with ant
RUN /u01/app/bccs/os_gem/app/apache-ant-1.10.5/bin/ant -f /u01/app/bccs/os_gem/app/src/build.xml
RUN ls -la /u01/app/bccs/os_gem/app/src/output/artifacts/customer_service_jar

# Copy resource folder to docker
COPY resources /u01/app/bccs/os_gem/resources

# Run app from jar file
CMD java -Duser.timezone=Asia/Ho_Chi_Minh -cp "src/output/artifacts/customer_service_jar/*:src/lib-fw/*:lib-backend/*" com.viettel.BackendApplication

