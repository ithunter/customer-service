-- Thông tin loại cấu hình
CREATE TABLE PS_CONFIG_TYPE (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	CODE NVARCHAR2(50),								-- Mã loại cấu hình
	NAME NVARCHAR2(200),							-- Tên loại cấu hình

	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật   
);
CREATE SEQUENCE PS_CONFIG_TYPE_SEQ;

 -- Thông tin cấu hình
CREATE TABLE PS_CONFIG (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	CODE NVARCHAR2(50),								-- Mã cấu hình	
	PS_CONFIG_TYPE_ID NUMBER(10),					-- Định danh loại cấu hình
	VALUE NVARCHAR2(1000),							-- Giá trị cấu hình
	DESCRIPTION NVARCHAR2(500),						-- Mô tả

	STATUS NUMBER(2),								-- Trạng thái (1: Hiệu lực, 0: Không hiệu lực)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_CONFIG_SEQ;

-- Thiết lập cấu hình hạn mức giao dịch
CREATE TABLE PS_LIMIT_CONFIG (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	PARTNER_CODE NUMBER(10),						-- Định danh đối tác (Hệ thống Party Mgmt)
	COMMAND_CODE NVARCHAR2(50),						-- Mã lệnh (API)
	POOL_LIMIT NUMBER(10),							-- Số lượng giao dịch tối đa trong 1 khoảng thời gian (Trong PS_CONFIG)

	STATUS NUMBER(2),								-- Trạng thái (1: Hiệu lực, 0: Không hiệu lực)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_LIMIT_CONFIG_SEQ;

-- Mapping đối tác với username
CREATE TABLE PS_PARTNER_INFO (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	USERNAME NVARCHAR2(50),							-- Tên đăng nhập đối tác
	PASSWORD NVARCHAR2(500),						-- Mật khẩu của đối tác (đã băm)
	PUBLIC_KEY CLOB,								-- Khóa công khai của đối tác

	PARTNER_ID NUMBER(10),							-- Định danh đối tác (Hệ thống Party Mgmt)
	PARTNER_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	PARTNER_TYPE NUMBER(1),							-- Loại đối tác (1 - Cá nhân, 2 - Tổ chức)

	STATUS NUMBER(2),								-- Trạng thái (1: Hiệu lực, 0: Không hiệu lực)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_PARTNER_INFO_SEQ;

-- Danh sách mã lệnh
CREATE TABLE PS_COMMAND (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	CODE NVARCHAR2(50),								-- Mã lệnh (API)
	NAME NVARCHAR2(200),							-- Tên lệnh
	DESCRIPTION NVARCHAR2(500),						-- Mô tả

	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_COMMAND_SEQ;

-- Danh sách mapping Partner - command
CREATE TABLE PS_MAP_PARTNER_COMMAND (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	PARTNER_ID NUMBER(10),							-- Định danh đối tác (Hệ thống Party Mgmt)
	COMMAND_ID NUMBER(10),							-- Định danh mã lệnh

	STATUS NUMBER(2),								-- Trạng thái (1: Cho phép, 0: Không cho phép)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_MAP_PARTNER_COMMAND_SEQ;

-- Danh sách mã dịch vụ
CREATE TABLE PS_SERVICE (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	CODE NVARCHAR2(50),								-- Mã dịch vụ
	NAME NVARCHAR2(200),							-- Tên dịch vụ
	DESCRIPTION NVARCHAR2(500),						-- Mô tả

	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_SERVICE_SEQ;

-- Danh sách mapping Partner - service
CREATE TABLE PS_MAP_PARTNER_SERVICE (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	PARTNER_ID NUMBER(10),							-- Định danh đối tác (Hệ thống Party Mgmt)
	SERVICE_ID NUMBER(10),							-- Định danh mã dịch vụ

	STATUS NUMBER(2),								-- Trạng thái (1: Cho phép, 0: Không cho phép)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_MAP_PARTNER_SERVICE_SEQ;

-- Danh sách ngân hàng hỗ trợ
CREATE TABLE PS_BANK (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	CODE NVARCHAR2(50),								-- Mã ngân hàng
	NAME NVARCHAR2(200),							-- Tên ngân hàng
	DESCRIPTION NVARCHAR2(500),						-- Mô tả

	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật 
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_BANK_SEQ;

-- Danh sách mapping parter - bank
CREATE TABLE PS_MAP_PARTNER_BANK (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	PARTNER_ID NUMBER(10),							-- Định danh đối tác (Hệ thống Party Mgmt)
	BANK_ID NUMBER(10),								-- Định danh ngân hàng

	STATUS NUMBER(2),								-- Trạng thái (1: Hiệu lực, 0: Không hiệu lực)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật
);
CREATE SEQUENCE PS_MAP_PARTNER_BANK_SEQ;

-- Lưu trữ lịch sử giao dịch với đối tác
CREATE TABLE PS_HISTORY (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	REQUEST_ID NVARCHAR2(200),						-- Mã giao dịch do PS sinh ra
	ORDER_ID NVARCHAR2(200),						-- Mã giao dịch do đối tác sinh ra

	PARTNER_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	COMMAND NVARCHAR2(150), 						-- Tên lệnh
	SERVICE_CODE NVARCHAR2(150),					-- Mã dịch vụ
	PROCESS_NAME NVARCHAR2(150),					-- Mô tả lệnh

	BILLING_CODE NVARCHAR2(150),					-- Mã thanh toán
	SERVER_PATH NVARCHAR2(150),						-- Đường dẫn server ứng dụng
	BANK_CODE NVARCHAR2(150),						-- Mã ngân hàng

	TRANS_AMOUNT NUMBER(30,2),						-- Số tiền
	MSISDN NVARCHAR2(20),							-- Số điện thoại được gửi từ đối tác
	
	ERROR_CODE_PS NVARCHAR2(150),					-- Mã lỗi trả về cho đối tác
	DESC_CODE_PS NVARCHAR2(150),					-- Mô tả mã lỗi trả về cho đối tác
	ERROR_DATE_PS DATE,								-- Thời điểm phản hồi mã lỗi cho đối tác

	SYSTEM_CODE NVARCHAR2(150),						-- Giá trị appCode
	PARTNER_TYPE NUMBER(2),							-- Loại đối tác (1: Shop, 2: Staff)
	REQUEST_CONTENT CLOB,							-- Nội dung bản tin gửi đi
	RESPONSE_CONTENT CLOB,							-- Nội dung bản nhận được

	ASYNC_DATE_PS DATE,								-- Thời điểm phản hồi bản tin bất đồng bộ cho đối tác
	CREATE_DATE DATE,								-- Thời điểm tạo
	UPDATE_DATE DATE								-- Thời điểm cập nhật
);
CREATE SEQUENCE PS_HIS_TPP_SEQ;

-- Lưu trữ lịch sử giao dịch gọi sang BCCS
CREATE TABLE PS_HISTORY_BCCS (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	REQUEST_ID NVARCHAR2(200),						-- Mã giao dịch do PS sinh ra
	TRANS_ID NVARCHAR2(200),						-- Mã giao dịch do BCCS sinh ra

	COMMAND NVARCHAR2(150), 						-- Tên lệnh
	COMMAND_ID NVARCHAR2(250), 						-- ID lệnh ( = REQUEST_ID + ID)
	SERVICE_CODE NVARCHAR2(150),					-- Mã dịch vụ

	OBJECT_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	OBJECT_TYPE NUMBER(2),							-- Loại đối tác (1: Shop, 2: Staff)
	REQ_TYPE NUMBER(2),								-- Loại giao dịch (1: TP - Topup, 2: PC - Thẻ cào)
	SYSTEM_CODE NVARCHAR2(150),						-- Giá trị appCode
	CONTRACT_ID NVARCHAR2(150),						-- Mã hợp đồng

	TRANS_AMOUNT NUMBER(30,2),						-- Số tiền giao dịch
	MSISDN NVARCHAR2(20),							-- Số điện thoại được gửi từ đối tác

	ERROR_CODE_BCCS NVARCHAR2(150),					-- Mã lỗi nhận được từ BCCS
	DESC_CODE_BCCS NVARCHAR2(150),					-- Mô tả mã lỗi nhận được từ BCCS
	ERROR_DATE_BCCS DATE,							-- Thời điểm nhận phản hồi mã lỗi của BCCS

	REQUEST_CONTENT CLOB,							-- Nội dung bản tin gửi đi
	RESPONSE_CONTENT CLOB,							-- Nội dung bản nhận được

	ASYNC_DATE_BCCS DATE,							-- Thời điểm nhận phản hồi bản tin bất đồng bộ từ BCCS
	CREATE_DATE DATE,								-- Thời điểm tạo
	UPDATE_DATE DATE								-- Thời điểm cập nhật
);
CREATE SEQUENCE PS_HISTORY_BCCS_SEQ;

-- Bảng cấu hình mua thẻ cào theo lô
CREATE TABLE PS_PINCODE_CONFIG (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	PARTNER_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	AMOUNT NUMBER(10),								-- Mệnh giá thẻ cào

	MAX_COUNT NUMBER(10),							-- Hạn mức thẻ cào tối đa cho phép đối tác tồn kho theo từng giao dịch của đối tác
	MAX_COUNT_PER_TRANS NUMBER(10),					-- Hạn mức thẻ cào tối đa cho từng giao dịch của đối tác

	STATUS NUMBER(2),								-- Trạng thái (1: Hiệu lực, 0: Không hiệu lực)
	CREATE_DATE DATE,								-- Thời điểm tạo 
	CREATE_USER NVARCHAR2(150),						-- Người dùng tạo
	UPDATE_DATE DATE,								-- Thời điểm cập nhật
	UPDATE_USER NVARCHAR2(150)						-- Người dùng cập nhật

);
CREATE SEQUENCE PS_PINCODE_CONFIG_SEQ;

-- Bảng lưu các giao dịch mua thẻ cào Viettel chưa cập nhật thông tin bán
CREATE TABLE PS_PINCODE_PENDING (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	REQUEST_ID NVARCHAR2(200),						-- Mã giao dịch do PS sinh ra
	ORDER_ID NVARCHAR2(200),						-- Mã giao dịch do đối tác sinh ra

	PARTNER_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	SERIAL_CODE NVARCHAR2(150),						-- Số serial thẻ cào

	AMOUNT NUMBER(10),								-- Mệnh giá thẻ cào
	SERVICE_CODE NVARCHAR2(150),					-- Mã dịch vụ

	CREATE_DATE DATE,								-- Thời điểm tạo
	UPDATE_DATE DATE								-- Thời điểm cập nhật
);
CREATE SEQUENCE PS_PINCODE_PENDING_SEQ;

-- Bảng lưu các giao dịch mua thẻ cào Viettel đã cập nhật thông tin bán
CREATE TABLE PS_PINCODE_UPDATE (
	ID NUMBER(20) PRIMARY KEY,						-- Khóa chính

	REQUEST_ID NVARCHAR2(200),						-- Mã giao dịch do PS sinh ra
	ORDER_ID NVARCHAR2(200),						-- Mã giao dịch do đối tác sinh ra

	PARTNER_CODE NVARCHAR2(50),						-- Mã đối tác (tương ứng với SHOP_CODE/STAFF_CODE trong BCCS_IM)
	SERIAL_CODE NVARCHAR2(150),						-- Số serial thẻ cào

	AMOUNT NUMBER(10),								-- Mệnh giá thẻ cào
	SERVICE_CODE NVARCHAR2(150),					-- Mã dịch vụ

	STAFF_ID NUMBER(10),							-- ID định danh nhân viên
	STAFF_MOBILE NVARCHAR2(20),						-- Số điện thoại nhân viên
	PAYER_MSISDN NVARCHAR2(20), 					-- Số điện thoại để nhắn tin thẻ cào đến khách hàng


	CREATE_DATE DATE,								-- Thời điểm tạo
	UPDATE_DATE DATE								-- Thời điểm cập nhật
);
CREATE SEQUENCE PS_PINCODE_UPDATE_SEQ;

-- Danh sách mapping mã lỗi với đối tác - TODO